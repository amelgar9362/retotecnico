package com.reto.tecnico.apiprestamo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolicitudDto {
	private Integer codigo;	
	private String tipoDocumento;
	
	private String numeroDocumento;
	
	private String nombres;
	
	private Double montoPrestamo;
	
	private Integer cuotas;

}
