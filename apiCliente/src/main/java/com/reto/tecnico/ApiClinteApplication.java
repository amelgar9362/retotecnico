package com.reto.tecnico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiClinteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiClinteApplication.class, args);
	}

}
