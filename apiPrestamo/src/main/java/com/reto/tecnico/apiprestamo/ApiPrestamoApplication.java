package com.reto.tecnico.apiprestamo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPrestamoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPrestamoApplication.class, args);
	}

}
