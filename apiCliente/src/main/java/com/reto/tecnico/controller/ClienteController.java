package com.reto.tecnico.controller;

import java.net.URI;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.tecnico.dto.ClienteDto;
import com.reto.tecnico.modelo.Cliente;
import com.reto.tecnico.service.IClienteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/client")
public class ClienteController {
	
	@Autowired
	IClienteService clienteService;
	
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Cliente>>>  findAll(){
		Flux<Cliente> fx = clienteService.findAll();
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(fx));
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Cliente>> findById(@PathVariable("id") String id){
		return clienteService.findById(id)
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p));
	}
	
	@PostMapping
	public Mono<ResponseEntity<Cliente>> save(@Valid @RequestBody ClienteDto cliente, final ServerHttpRequest req){
		cliente.setNombres(cliente.getNombres().toUpperCase());
		cliente.setTipoDocumento(cliente.getTipoDocumento().toUpperCase());
		
		Cliente clienteModel = mapper.map(cliente, Cliente.class);
		return clienteService.save(clienteModel)
				.map( p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getCodigo())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(p));
	}
	
	@PutMapping("/{id}")
	public Mono<ResponseEntity<Cliente>> update(@PathVariable("id") String id,@Valid @RequestBody ClienteDto cliente){
		
		Cliente clienteModel = mapper.map(cliente, Cliente.class);
		Mono<Cliente> monoBody = Mono.just(clienteModel);
		Mono<Cliente> monoBD = clienteService.findById(id);
		return monoBD.zipWith(monoBody, (bd, ps) -> {
					bd.setCodigo(id);
					bd.setNombres(ps.getNombres().toUpperCase());
					bd.setTipoDocumento(ps.getTipoDocumento().toUpperCase());
					bd.setNumeroDocumento(ps.getNumeroDocumento());
					return bd;
				})
				.flatMap(clienteService::update)
				.map(y -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON).
						body(y))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

}
