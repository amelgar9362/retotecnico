package com.reto.tecnico.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.reto.tecnico.modelo.Cliente;

public interface IClienteDao extends ReactiveMongoRepository<Cliente, String> {

}
