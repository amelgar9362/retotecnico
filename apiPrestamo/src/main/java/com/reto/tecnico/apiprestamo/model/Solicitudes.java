package com.reto.tecnico.apiprestamo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "solicitud")
@Data
public class Solicitudes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer codigo;
	
	@Column(name = "tipo_documento")
	private String tipoDocumento;
	@Column(name = "numero_documento")
	private String numeroDocumento;
	@Column(name = "nombres")
	private String nombres;
	@Column(name = "monto_prestamo")
	private Double montoPrestamo;
	@Column(name = "cuota")
	private Integer cuotas;

}
