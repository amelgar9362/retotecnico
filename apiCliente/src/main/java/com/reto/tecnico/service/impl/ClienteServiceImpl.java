package com.reto.tecnico.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reto.tecnico.dao.IClienteDao;
import com.reto.tecnico.modelo.Cliente;
import com.reto.tecnico.service.IClienteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ClienteServiceImpl implements IClienteService {
	
	@Autowired
	IClienteDao clienteDao;

	@Override
	public Mono<Cliente> save(Cliente cliente) {
		// TODO Auto-generated method stub
		return clienteDao.save(cliente);
	}

	@Override
	public Mono<Cliente> update(Cliente cliente) {
		// TODO Auto-generated method stub
		return clienteDao.save(cliente);
	}

	@Override
	public Flux<Cliente> findAll() {
		// TODO Auto-generated method stub
		return clienteDao.findAll();
	}

	@Override
	public Mono<Cliente> findById(String id) {
		// TODO Auto-generated method stub
		return clienteDao.findById(id);
	}

}
