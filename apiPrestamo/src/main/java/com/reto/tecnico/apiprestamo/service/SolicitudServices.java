package com.reto.tecnico.apiprestamo.service;

import com.reto.tecnico.apiprestamo.model.Solicitudes;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SolicitudServices {
	Mono<Solicitudes> save(Solicitudes solicitud);
	Mono<Solicitudes> update(Solicitudes solicitud);
	Mono<Solicitudes> findById(Integer id);
	Flux<Solicitudes> findAll();

}
