package com.reto.tecnico.apiPrestamo.controller;

import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.WebProperties.Resources;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.reto.tecnico.apiprestamo.controller.SolicitudController;
import com.reto.tecnico.apiprestamo.dao.ISolicitudesDao;
import com.reto.tecnico.apiprestamo.dto.SolicitudDto;
import com.reto.tecnico.apiprestamo.model.Solicitudes;
import com.reto.tecnico.apiprestamo.service.impl.SolicitudServiceImpl;

import reactor.core.publisher.Mono;


@WebFluxTest(controllers = SolicitudController.class)
@Import(SolicitudServiceImpl.class)
public class SolicitudControllerTest {
	@Autowired
    //@Qualifier("customWebTestClient")
    private WebTestClient client;

    @MockBean
    private ISolicitudesDao repo;

    @MockBean
    private Resources resources;

    @Test
    void findAll(){
        Solicitudes dto = new Solicitudes();
        dto.setCodigo(1);
        dto.setCuotas(24);
        dto.setMontoPrestamo(25000.00);
        dto.setNombres("xXXXXxxX");
        dto.setNumeroDocumento("DNI");
        dto.setTipoDocumento("12345678");

        Solicitudes dto2 = new Solicitudes();
        dto2.setCodigo(1);
        dto2.setCuotas(24);
        dto2.setMontoPrestamo(28000.00);
        dto2.setNombres("xXXXXxxX");
        dto2.setNumeroDocumento("DNI");
        dto2.setTipoDocumento("12345698");

        List<Solicitudes> list = new ArrayList<>();
        list.add(dto);
        list.add(dto2);

        Mockito.when(repo.findAll()).thenReturn(list);

        client.get()
                .uri("/solicitud")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Solicitudes.class)
                .hasSize(2);
    }

    @Test
    void createTest(){
    	Solicitudes dto2 = new Solicitudes();
        dto2.setCodigo(1);
        dto2.setCuotas(24);
        dto2.setMontoPrestamo(28000.00);
        dto2.setNombres("xXXXXxxX");
        dto2.setNumeroDocumento("DNI");
        dto2.setTipoDocumento("12345698");

        Mockito.when(repo.save(any())).thenReturn(dto2);

        client.post()
                .uri("/solicitud")
                .body(Mono.just(dto2), SolicitudDto.class)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.nombres").isNotEmpty()
                .jsonPath("$.cuotas").isNumber();
    }

}
