package com.reto.tecnico.apiprestamo.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reto.tecnico.apiprestamo.dao.ISolicitudesDao;
import com.reto.tecnico.apiprestamo.model.Solicitudes;
import com.reto.tecnico.apiprestamo.service.SolicitudServices;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class SolicitudServiceImpl implements SolicitudServices{
	
	@Autowired
	ISolicitudesDao solicitudesDao;

	@Override
	public Mono<Solicitudes> save(Solicitudes solicitud) {
		// TODO Auto-generated method stub
		Solicitudes soli=solicitudesDao.save(solicitud);
		
		return Mono.just(soli);
	}

	@Override
	public Mono<Solicitudes> update(Solicitudes solicitud) {
		// TODO Auto-generated method stub
		Solicitudes soli=solicitudesDao.save(solicitud);
		return Mono.just(soli);
	}

	@Override
	public Mono<Solicitudes> findById(Integer id) {
		// TODO Auto-generated method stub
		Optional<Solicitudes> dto = solicitudesDao.findById(id);
		return Mono.just(dto.get());
	}

	@Override
	public Flux<Solicitudes> findAll() {
		// TODO Auto-generated method stub
		Iterable<Solicitudes> list =  solicitudesDao.findAll();
		return Flux.fromIterable(list);
	}

}
