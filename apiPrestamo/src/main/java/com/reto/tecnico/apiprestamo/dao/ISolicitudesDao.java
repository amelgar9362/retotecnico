package com.reto.tecnico.apiprestamo.dao;

import org.springframework.data.repository.CrudRepository;

import com.reto.tecnico.apiprestamo.model.Solicitudes;

public interface ISolicitudesDao extends CrudRepository<Solicitudes, Integer> {

}
