package com.reto.tecnico.apiprestamo.controller;

import java.net.URI;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reto.tecnico.apiprestamo.dto.SolicitudDto;
import com.reto.tecnico.apiprestamo.model.Solicitudes;
import com.reto.tecnico.apiprestamo.service.SolicitudServices;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/solicitud")
public class SolicitudController {
	
	@Autowired
	SolicitudServices solicitudServices;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping
	public Mono<ResponseEntity<Flux<SolicitudDto>>>  findAll(){
		Flux<Solicitudes> fx = solicitudServices.findAll();
		Flux<SolicitudDto> dto = fx.map(p -> mapper.map(p, SolicitudDto.class));
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(dto));
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<SolicitudDto>> findById(@PathVariable("id") Integer id){
		return solicitudServices.findById(id)
				.map(j -> mapper.map(j, SolicitudDto.class))
				.map(p -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON)
						.body(p));
	}
	
	@PostMapping
	public Mono<ResponseEntity<SolicitudDto>> save(@Valid @RequestBody SolicitudDto dto, final ServerHttpRequest req){
		Solicitudes model = mapper.map(dto, Solicitudes.class);
		return solicitudServices.save(model)
				.map(j -> mapper.map(j, SolicitudDto.class))
				.map( p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getCodigo().toString())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(p));
	}
	
	@PutMapping("/{id}")
	public Mono<ResponseEntity<SolicitudDto>> update(@PathVariable("id") Integer id,@Valid @RequestBody SolicitudDto dto){
		
		Solicitudes model = mapper.map(dto, Solicitudes.class);
		Mono<Solicitudes> monoBody = Mono.just(model);
		Mono<Solicitudes> monoBD = solicitudServices.findById(id);
		return monoBD.zipWith(monoBody, (bd, ps) -> {
					bd.setCodigo(id);
					bd.setNombres(ps.getNombres().toUpperCase());
					bd.setTipoDocumento(ps.getTipoDocumento().toUpperCase());
					bd.setNumeroDocumento(ps.getNumeroDocumento());
					bd.setMontoPrestamo(ps.getMontoPrestamo());
					bd.setCuotas(ps.getCuotas());
					return bd;
				})
				.flatMap(solicitudServices::update)
				.map(j -> mapper.map(j, SolicitudDto.class))
				.map(y -> ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON).
						body(y))
				.defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	

}
