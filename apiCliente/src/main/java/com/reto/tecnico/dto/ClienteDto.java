package com.reto.tecnico.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class ClienteDto {
	
	private String codigo;
	@NotNull
	@Min(value = 3)
	private String tipoDocumento;
	
	@NotNull
	@Pattern(regexp = "[0-9]{8}", message = "numeroDocumento tiene que tener 8 digitos")
	private String numeroDocumento;
	@NotNull
	@Min(value = 5)
	private String nombres;

}
