package com.reto.tecnico.service;

import com.reto.tecnico.modelo.Cliente;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IClienteService {
	Mono<Cliente> save(Cliente cliente);
	Mono<Cliente> update(Cliente cliente);
	Flux<Cliente> findAll();
	Mono<Cliente> findById(String id);

}
